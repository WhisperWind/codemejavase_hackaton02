package codeme.hackaton02.shoppinglist.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "pb_shopping_list")
public class ShoppingList {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id; 
	private String name;
	@Column(name="creation_date")
    private Timestamp creationDate;
	
	@ManyToMany
    @JoinTable(name="pb_shopping_list_product",
    	joinColumns = 
    		@JoinColumn(name="shopping_list_id"),
    	inverseJoinColumns=
            @JoinColumn(name="product_id")
    )
    private List<Product> products;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
}
