package codeme.hackaton02;

import codeme.hackaton02.Lacz;

public class Logger extends Exception {

    private static final long serialVersionUID = 2997916675319132730L;
    
    private boolean cause = false;
    private boolean local = false;
    private boolean stack = false;
    
    
    public Logger(String msg){
    	super(msg);
    }
    
    @Override
    public String getMessage() {
        
        String message = super.getMessage();
        String log = "";
        
        log += "Message: " + message;
        
        if (this.local) {
            log += "\nLocalized message: " + super.getLocalizedMessage();
        }
        
        if (this.cause) {
            log += "\nCause: " + super.getCause().toString();
        }
        
        if (this.stack) {
            log += "\nStack trace: " + super.getStackTrace();
        }
        

        Lacz.save(log);
        
        return log;
    }

//    public String getMessage(boolean cause, boolean local, boolean stack) {
//        
//        this.cause = cause;
//        this.local = local;
//        this.stack = stack;
//        
//        return getMessage();
//        
//    }
}