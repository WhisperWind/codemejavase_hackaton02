package codeme.hackaton02;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class Lacz {
    public static void main(String[] args) {
           save("WOW!!!");
           printlog();
    }
    
    
    public static void save(String comment){
        try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/codeme2", "root", "ancekjaz15");
            Statement statement = conn.createStatement();
            
            statement.executeUpdate("INSERT INTO logs (datalog, note) VALUES (now(), '"+ comment +  "');");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
    
    }
    
    public static void printlog(){
        try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/codeme2", "root", "ancekjaz15");

			PreparedStatement stm = conn.prepareStatement("SELECT * FROM logs");
            ResultSet res = stm.executeQuery();
            ResultSetMetaData md = res.getMetaData();
            while(res.next()) {
                for(int ix = 1; ix <= md.getColumnCount(); ix++) {
                    System.out.println(
                        md.getColumnName(ix) + " - " + 
                        md.getColumnTypeName(ix) + " - " + 
                        res.getString(md.getColumnName(ix))
                    );
                }
            }

            res.close();
            stm.close();
            conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
    }
}

