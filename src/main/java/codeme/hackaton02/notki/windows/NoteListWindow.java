package codeme.hackaton02.notki.windows;

import java.util.List;

import codeme.hackaton02.notki.entity.Note;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NoteListWindow extends Stage{

	public NoteListWindow(String title, Stage parent, List<Note> notes) {
		
		NoteListWindow window = this;
		this.setTitle(title);
		
		FlowPane rootNode = new FlowPane();
		
		VBox vbox = new VBox();
		
		if (notes.isEmpty()) {
			// TODO zrobić refactor by wyświetlał ten alert i wrócił do okna głownego
			Alert notesEmptyAlet = new Alert(AlertType.INFORMATION);
			notesEmptyAlet.setContentText("Brak notatek");
			notesEmptyAlet.showAndWait();
			
		}
		
		ObservableList<Note> noteList = FXCollections.observableArrayList();
		
		for (Note note : notes) {
			noteList.add(note);
		}
		
		ListView<Note> list = new ListView<>(noteList);
		
		vbox.getChildren().add(list);
		
		
		Button returnBT = new Button("Powrót");
		vbox.getChildren().add(returnBT);
		rootNode.getChildren().add(vbox);
		
		returnBT.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				window.close();
				parent.show();
			}
		});
		
		Scene scene = new Scene(rootNode, 400, 500);
		this.setScene(scene);
	}
}
