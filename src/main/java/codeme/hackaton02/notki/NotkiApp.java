package codeme.hackaton02.notki;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import codeme.hackaton02.notki.entity.Note;
import codeme.hackaton02.notki.entity.NoteManager;
import codeme.hackaton02.notki.windows.NoteListWindow;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NotkiApp extends Application {
	
	private static NoteManager nm = NoteManager.getInstance();

	public static void main(String[] args) {

//		NoteManager nm = NoteManager.getInstance();
		
		Note noteToAdd = new Note();
		java.util.Date date= new java.util.Date();
//		DateTime data = new DateTime(); // bobranie bieżącej daty
		
		noteToAdd.setCreationDate(new Timestamp(date.getTime()));
		System.out.println("notka.date: " + noteToAdd.getCreationDate());
		String content = "notka wpisana 1";
		noteToAdd.setNote(content);
		
//		nm.addNote(noteToAdd);
		
		List<Note> notes = nm.getNotes();
		for (Note note : notes) {
			System.out.println("notka: " + note.getNote());
		}
		
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Notki");
		FlowPane flowPane = new FlowPane();
		
		Button addNoteBT = new Button("Dodaj notatkę");
		Button showNotesBT = new Button("Pokaż wyszystkie notatki");
		
		VBox dateVBox = new VBox();
		Button showNotesByDateBT = new Button("Wybierz notki od do");
		Label dateFromLB = new Label("Data od:");
		Label dateToLB = new Label("Data do:");
		DatePicker dateFromDP = new DatePicker(LocalDate.now());
		DatePicker dateToDP = new DatePicker(LocalDate.now());
		
//		Timestamp dateFromT = new Timestamp(1).valueOf(dateFromDP.getValue().toString());
		
/*
		Date data = new Date();
		LocalDate localDate = dateFromDP.getValue();
		Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
*/
		
		/*
		LocalDateTime ldt = new LocalDateTime();
		DateTimeFormatter dtf = DateTimeFormatter.forPattern("yyyy-MM-dd HH:mm:ss");
		Timestamp ts = Timestamp.valueOf(ldt.toString(dtf));
		*/
		
		LocalDate localDate = dateFromDP.getValue();
		Date date = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
		Timestamp timeStamp = new Timestamp(date.getTime());
		
		
		System.out.println("TS " + timeStamp.toString());
		
		System.out.print("dateFrom: " + dateFromDP.getValue());
		System.out.print("dateFromT: " + dateFromDP.getValue());
		
		showNotesBT.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				
				List<Note> notes = nm.getNotes();
				NoteListWindow window = new NoteListWindow("Lista Notatek", stage, notes);
				window.show();
				stage.hide();
			}
		});
		
		showNotesByDateBT.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				//konwersja z LocalDate(z DatePicker) na Timestamp
				LocalDate localDate = dateFromDP.getValue();
				Date date = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
				Timestamp dateFrom = new Timestamp(date.getTime());
				
				//konwersja z LocalDate(z DatePicker) na Timestamp
				localDate = dateToDP.getValue();
				date = Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
				Timestamp dateTo = new Timestamp(date.getTime());
				
				
				List<Note> notes = nm.getNotesByDates(dateFrom, dateTo);
				
				NoteListWindow window = new NoteListWindow("Lista Notatek", stage, notes);
				window.show();
				stage.hide();
				
			}
		});
		
		
		
		
		flowPane.getChildren().add(addNoteBT);
		flowPane.getChildren().add(showNotesBT);
		
		dateVBox.getChildren().add(dateFromLB);
		dateVBox.getChildren().add(dateFromDP);
		dateVBox.getChildren().add(dateToLB);
		dateVBox.getChildren().add(dateToDP);
		dateVBox.getChildren().add(showNotesByDateBT);
		flowPane.getChildren().add(dateVBox);
		
		Scene scene = new Scene(flowPane, 1024, 786);
		stage.setScene(scene);
		stage.show();
	}

}
