package codeme.hackaton02.notki.entity;

import java.sql.Timestamp;

public class Note {

	private Integer id;
	private Timestamp creationDate;
	private String note;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String content) {
		this.note = content;
	}
	
	public String toString() {
		return note;
	}
}
