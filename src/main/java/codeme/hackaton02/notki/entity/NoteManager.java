package codeme.hackaton02.notki.entity;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

public class NoteManager {

	private static NoteManager noteManager;
	private static Connection conn;
	
	/*
	private NoteManager noteManager;
	private Connection conn;
	*/
	private NoteManager() {
		try {
			conn = (Connection) DriverManager.getConnection("jdbc:mysql://devht.pl/codeme", "codeme", "codeme123");
			
		} catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public static NoteManager getInstance() {
		if (noteManager == null) noteManager = new NoteManager(); 
		return noteManager;
	}
	
	public List<Note> getNotes() {
		List<Note> notes = new ArrayList<>();
		Statement stm = null;
        PreparedStatement pstm = null;
        ResultSet res = null;
        
        try {
        	stm = conn.createStatement();
            res = stm.executeQuery("SELECT * FROM pb_note");
            while (res.next()) {
            	Note note = new Note();
            	note.setId(res.getInt("id"));
            	note.setCreationDate(res.getTimestamp("creation_date"));
            	note.setNote(res.getString("note"));
            	notes.add(note);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            try {
                res.close();
                if(pstm != null) pstm.close();
                if(stm != null) stm.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
		return notes;
	}
	
	public List<Note> getNotesByDates(Timestamp dateFrom, Timestamp dateTo) {
		List<Note> notes = new ArrayList<>();
		Statement stm = null;
        PreparedStatement pstm = null;
        ResultSet res = null;
        
        try {
            
            pstm = conn.prepareStatement("SELECT * FROM pb_note WHERE creation_date BETWEEN ? AND ?");
        	pstm.setTimestamp(1, dateFrom);
        	pstm.setTimestamp(2, dateTo);
        	res = pstm.executeQuery();
            
            while (res.next()) {
            	Note note = new Note();
            	note.setId(res.getInt("id"));
            	note.setCreationDate(res.getTimestamp("creation_date"));
            	note.setNote(res.getString("note"));
            	notes.add(note);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            try {
                res.close();
                if(pstm != null) pstm.close();
                if(stm != null) stm.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
		return notes;
	}
	
	public void addNote(Note note){
		Statement stm = null;
        PreparedStatement pstm = null;
        ResultSet res = null;
        
        try {
        	
        	pstm = conn.prepareStatement("INSERT INTO `codeme`.`pb_note`(`creation_date`, `note`) VALUES (?, ?)");
        	pstm.setTimestamp(1, note.getCreationDate());
        	pstm.setString(2, note.getNote());
        	pstm.executeUpdate();
        	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            try {
                if(pstm != null) pstm.close();
                if(stm != null) stm.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}
	
	public Note getNotebyID(int id){
		Note note = new Note();
		
		Statement stm = null;
        PreparedStatement pstm = null;
        ResultSet res = null;
        
        try {
        	
        	pstm = conn.prepareStatement("SELECT * FROM codeme.pb_note WHERE id = ?");
        	pstm.setInt(1, id);
        	res = pstm.executeQuery();
        	res.next();
        	
        	note.setId(res.getInt("id"));
        	note.setCreationDate(res.getTimestamp("creation_date"));
        	note.setNote(res.getString("note"));
        	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
            try {
                if(pstm != null) pstm.close();
                if(stm != null) stm.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		
		return note;
	}
	
	public void disconnect() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void finalize() throws Throwable {
        this.disconnect();
        super.finalize();
    }
}
