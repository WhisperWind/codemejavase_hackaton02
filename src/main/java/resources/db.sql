create table pb_note (
	id int not null auto_increment, 
    creation_date timestamp , 
	note text, 
    primary key (id)
);

create table pb_product (
	id int not null auto_increment, 
    name varchar(100) not null,
    price double(7,4) not null default 0.0,
    primary key (id)
);

create table pb_shopping_list(
	id int not null auto_increment, 
    name varchar(100) not null,
    creation_date timestamp , 
    primary key (id)
);

create table pb_shopping_list_product(
	id int not null auto_increment, 
    shopping_list_id int not null references pb_shopping_list.id,
    product_id int not null references pb_product.id,
    primary key (id)
);